## Développement d'une ressource *pizza*

### API et représentation des données


| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                       |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------         |
| /pizza                  | GET         | <-application/json<br><-application/xml                      |                 | Liste des pizzas                                          |
| /pizza/{id}             | GET         | <-application/json<br><-application/xml                      |                 | Une pizzas ou 404                                        |
| /pizza/{id}/name        | GET         | <-text/plain                                                 |                 | Le nom de la pizzas ou 404                                    |
| /pizza                  | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza      | Nouvelle pizza (P2)<br>409 si la pizza existe déjà (même nom) |
| /pizza/{id}             | DELETE      | <-application/json<br><-application/xml                      |                 | Supprime une pizza                                            |


Représentation d'une pizza avec le nom:

    {
        "name": "margarita"
    }

Représentation d'une pizza avec un id, un nom et une liste d'id d'ingredients

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "mozzarella"
      "ingredients" : [{f38806a8-7c85-49ef, 980c-149dcd81d306}]
    }