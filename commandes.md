## Développement d'une ressource *commande*

### API et représentation des données


| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                 >
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :--------------------------------------->
| /commande               | GET         | <-application/json<br><-application/xml                      |                 | Liste des commandes                      >
| /commande/{id}             | GET         | <-application/json<br><-application/xml                      |                 | Une commande ou 404                   >
| /commande                  | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Commande      | Nouvelle commande
| /commande/{id}             | DELETE      | <-application/json<br><-application/xml                      |                 | Supprime une commande
| /commande/{id}/pizzas             | GET      | <-application/json<br><-application/xml                      |                 | Liste de pizzas
| /commande/{id}/pizzas/{id}             | GET      | <-application/json<br><-application/xml                      |                 | La pizza avec l'id correspond>


Représentation d'une commande avec un id (généré aleatoirement) :
    {
        "id": "f54961a8-9u24-26vb"
    }

Représentation d'une commande avec un id et une liste de pizzas (id généré aléatoirement ):
    {
      "id": "f54961a8-9u24-26vb",
      "pizza" : [{f38806a8-7c85-49ef, 980c-149dcd81d306}]
    }