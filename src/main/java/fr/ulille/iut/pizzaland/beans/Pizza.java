package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
    private UUID id = UUID.randomUUID();
    private String name;
    private List<Ingredient> ingredients;

    public Pizza() {
    }

    public Pizza(String name) {
        this.name = name;
        this.ingredients = new ArrayList<>();
    }

    public Pizza(UUID id, String name) {
        this.id = id;
        this.name = name;
        this.ingredients = new ArrayList<>();
    }

    public Pizza(UUID id, String name, List<Ingredient> ingredients) {
		super();
		this.id = id;
		this.name = name;
		this.ingredients = ingredients;
	}

    public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
        dto.setId(p.getId());
        dto.setName(p.getName());
        dto.setIngredients(p.getIngredients());

        return dto;
    }
    
    public static Pizza fromDto(PizzaDto dto) {
    	Pizza pizzas = new Pizza();
    	pizzas.setId(dto.getId());
    	pizzas.setName(dto.getName());
    	pizzas.setIngredients(dto.getIngredients());
        return pizzas;
    }
    
    public static PizzaCreateDto toCreateDto(Pizza pizza) {
    	PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(pizza.getName());
            
        return dto;
      }
    	
      public static Pizza fromCreateDto(PizzaCreateDto dto) {
    	  Pizza pizzas = new Pizza();
    	  pizzas.setName(dto.getName());

        return pizzas;
      }
}