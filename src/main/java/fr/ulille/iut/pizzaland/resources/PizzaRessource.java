package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Path("/pizzas")
public class PizzaRessource {
	private static final Logger LOGGER = Logger.getLogger(PizzaRessource.class.getName());
	private PizzaDao pizzas;

	@Context
	public UriInfo uriInfo;

	public PizzaRessource() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createTables();
	}

	@GET
	public List<PizzaDto> getAll() {
		LOGGER.info("PizzaRessource:getAll");
		List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public PizzaDto getOnePizza(@PathParam("id") UUID id) {
		LOGGER.info("getOnePizza(" + id + ")");
		try {
			Pizza ingredient = pizzas.findById(id);
			LOGGER.info(ingredient.toString());
			return Pizza.toDto(ingredient);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@POST
	public Response createPizza(PizzaCreateDto pizzaCreateDto) {
		Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
		if (existing != null)
			throw new WebApplicationException(Response.Status.CONFLICT);
		try {
			Pizza p = Pizza.fromCreateDto(pizzaCreateDto);
			pizzas.insertPizza(p);
			PizzaDto pizzaDto = Pizza.toDto(p);
			URI uri = uriInfo.getAbsolutePathBuilder().path(p.getId().toString()).build();
			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	@DELETE
	@Path("{id}")
	public Response deletePizza(@PathParam("id") UUID id) {
		if ( pizzas.findById(id) == null )
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		pizzas.removePizza(id);
		return Response.status(Response.Status.ACCEPTED).build();
	}

	@GET
	@Path("{id}/name")
	public String getPizzaName(@PathParam("id") UUID id) {
		Pizza p = pizzas.findById(id);
		if (p == null)
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		return p.getName();
	}
}